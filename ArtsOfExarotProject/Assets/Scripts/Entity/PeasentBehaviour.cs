﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeasentBehaviour : AEntityBehaviour
{
    public float gatherSpeed;

    public int maxResourceAmount;

    public int currentResourceAmountCarry;

    public ResourceBehaviour currentResource;

    public ResourceType resourceType = ResourceType.People;

    public ResourceType resourceTypeThatIsCarrying = ResourceType.None;

    private DepositBehaviour[] allDepositsFounded;


    private void OnEnable()
    {
        EventManager.buildingPosition += OnBuildingPosition;
        EventManager.resourceDestroyed += OnResourceDestroyed;
    }

    public void GoToNearestDeposit() 
    {
        allDepositsFounded = FindObjectsOfType<DepositBehaviour>();

        List<DepositBehaviour> currentResourceDeposits = new List<DepositBehaviour>();

        DepositBehaviour temp = new DepositBehaviour();

        for (int i = 0; i < allDepositsFounded.Length; i++)
        {
            if(allDepositsFounded[i].depositResource == resourceTypeThatIsCarrying)
            {
                currentResourceDeposits.Add(allDepositsFounded[i]);
                //Debug.Log($"founded a valid deposit");
            }
        }

        if (currentResourceDeposits.Count > 0)
        {
            for (int i = 0; i < currentResourceDeposits.Count - 1; i++)
            {
                for (int j = 0; j < currentResourceDeposits.Count - 1; j++)
                {
                    if (Vector3.Distance(currentResourceDeposits[i].transform.position, transform.position) >
                    Vector3.Distance(currentResourceDeposits[i + 1].transform.position, transform.position))
                    {
                        temp = currentResourceDeposits[i];
                        currentResourceDeposits[i] = currentResourceDeposits[i + 1];
                        currentResourceDeposits[i + 1] = temp;
                        //Debug.Log($"{currentResourceDeposits[i]} is nearest " +
                        //    $"[distance of {Vector3.Distance(currentResourceDeposits[i].transform.position, transform.position)}] " +
                        //    $"then {currentResourceDeposits[i].depositResource} " +
                        //    $"[distance of {Vector3.Distance(currentResourceDeposits[i + 1].transform.position, transform.position)}]");
                    }
                }      
            }

            GameObject destination = currentResourceDeposits[0].gameObject;
            Movement(destination.transform.position);
        }
        else
        {
            GameObject destination = GameObject.FindGameObjectWithTag("TownHall");

            Movement(destination.transform.position);
        }
    }

    private void OnDisable()
    {
        EventManager.buildingPosition -= OnBuildingPosition;
        EventManager.resourceDestroyed -= OnResourceDestroyed;
    }

    private void OnBuildingPosition(Vector3 bpPosition)
    {
        if(currentUnitState == EntityState.Selected)
        {
            Movement(bpPosition);
        }
    }

    private void OnResourceDestroyed()
    {
        if (currentResourceAmountCarry > 0)
        {
            GoToNearestDeposit();
        }
        else
        {
            Movement(transform.position);
            currentUnitState = EntityState.Idle;
            resourceTypeThatIsCarrying = ResourceType.None;
            //Debug.Log("Stop Right There");
        }
    }
}
