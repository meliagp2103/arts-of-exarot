﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class AEntityBehaviour : MonoBehaviour,IDamageable,IMultipleSelectable
{
    public EntityStats stats;

    public GameObject selectedImage;

    public resourceCost unitCost;

    public EntityType type;

    protected NavMeshAgent agent;

    public EntityState currentUnitState;

    public UIIdentifier UIID;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    //maybe a vector3 that indicates the destination of the object
    public virtual void Movement(Vector3 positionToReach)
    {
        currentUnitState = EntityState.Moving;
        agent.SetDestination(positionToReach);

        //put this in a while
        //Debug.Log(Vector3.Distance(transform.position, positionToReach));

        if(Vector3.Distance(transform.position,positionToReach) <= agent.stoppingDistance)
        {
            currentUnitState = EntityState.Idle;
        }
    }

    public void TakeDamage(float dmgAmount)
    {
        throw new System.NotImplementedException();
    }

    public virtual void Select()
    {
        currentUnitState = EntityState.Selected;
        EventManager.updateSelectedUI?.Invoke(UIID);
        selectedImage.SetActive(true);
    }

    public virtual void Deselect()
    {
        currentUnitState = EntityState.Idle;
        EventManager.updateSelectedUI?.Invoke(UIID);
        selectedImage.SetActive(false);
    }
}

public enum EntityState
{
    Idle,
    Selected,
    Task,
    Gathering,
    Depositing,
    Moving,
    Attack
}
