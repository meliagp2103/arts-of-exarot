﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{

    public float panSpeed = 20f;

    public float panBorderThickness = 10f;

    public float scrollSpeed = 200;

    public Vector2 cameraMovementLimit;

    public float minScroll;

    public float maxScroll;


    // Update is called once per frame
    void Update()
    {

        Vector3 currentPosition = transform.position;

        if (Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            currentPosition.z += panSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S) || Input.mousePosition.y <= panBorderThickness)
        {
            currentPosition.z -= panSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D) || Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            currentPosition.x += panSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.A) || Input.mousePosition.x <= panBorderThickness)
        {
            currentPosition.x -= panSpeed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");

        currentPosition.y -= scroll * scrollSpeed * Time.deltaTime;

        currentPosition.x = Mathf.Clamp(currentPosition.x, -cameraMovementLimit.x, cameraMovementLimit.x);

        currentPosition.y = Mathf.Clamp(currentPosition.y, minScroll, maxScroll);

        currentPosition.z = Mathf.Clamp(currentPosition.z, -cameraMovementLimit.y, cameraMovementLimit.y);

        transform.position = currentPosition;
    }
}
