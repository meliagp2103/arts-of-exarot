﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourceDisplayer : MonoBehaviour
{

    public TextMeshProUGUI foodText;
    public TextMeshProUGUI woodText;
    public TextMeshProUGUI stoneText;
    public TextMeshProUGUI peopleText;
    public TextMeshProUGUI goldText;


    private void OnEnable()
    {
        EventManager.displayResource += OnDisplayResource;
    }

    private void OnDisable()
    {
        EventManager.displayResource -= OnDisplayResource;
    }


    public void OnDisplayResource(ResourceType resource,int amount)
    {
        switch (resource)
        {
            case ResourceType.None:
                break;
            case ResourceType.People:
                peopleText.text = amount.ToString();
                break;
            case ResourceType.Food:
                foodText.text = amount.ToString();
                break;
            case ResourceType.Gold:
                goldText.text = amount.ToString();
                break;
            case ResourceType.Stone:
                stoneText.text = amount.ToString();
                break;
            case ResourceType.Wood:
                woodText.text = amount.ToString();
                break;
        }
    }

}
