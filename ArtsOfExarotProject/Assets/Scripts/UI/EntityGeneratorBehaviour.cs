﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityGeneratorBehaviour : MonoBehaviour
{
    public float timeToGenerate;
    public GameObject[] entitiesToGenerate;
    public GameObject spawner;
    public Vector3 positionHelper;

    //// Start is called before the first frame update
    //void Start()
    //{
        
    //}

    //// Update is called once per frame
    //void Update()
    //{
        
    //}

    public IEnumerator CountDownToGeneration(int indexToGenerate)
    {
        yield return new WaitForSeconds(timeToGenerate);
        GameObject newPanw = Instantiate(entitiesToGenerate[indexToGenerate]);
        newPanw.transform.position = new Vector3(spawner.transform.position.x + positionHelper.x, 
            spawner.transform.position.y + positionHelper.y, 
            spawner.transform.position.z + positionHelper.z);
    }

    public void GenerateUnit(int indexToGenerate)
    {
        StartCoroutine(CountDownToGeneration(indexToGenerate));
    }
}
