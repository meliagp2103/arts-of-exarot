﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : ASingleton<UIManager>
{
    public GameObject townHallSelectedUI;
    public GameObject unitSelectedUI;

    private void OnEnable()
    {
        EventManager.updateSelectedUI += OnUpdateSelectedUI;
    }

    private void OnDisable()
    {
        EventManager.updateSelectedUI -= OnUpdateSelectedUI;
    }

    public void OnUpdateSelectedUI(UIIdentifier id)
    {
        switch (id)
        {
            case UIIdentifier.None:
                break;
            case UIIdentifier.TownHall:
                if (!townHallSelectedUI.activeSelf)
                {
                    townHallSelectedUI.SetActive(true);
                }
                else
                {
                    townHallSelectedUI.SetActive(false);
                }
                break;
            case UIIdentifier.Peasent:
                if (!unitSelectedUI.activeSelf)
                {
                    unitSelectedUI.SetActive(true);
                }
                else
                {
                    unitSelectedUI.SetActive(false);
                }
                break;
        }
    }

}
public enum UIIdentifier
{
    None,
    TownHall,
    Peasent,
}
