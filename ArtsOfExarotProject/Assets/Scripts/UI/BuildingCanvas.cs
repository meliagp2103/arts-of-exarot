﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCanvas : MonoBehaviour
{
    [SerializeField]
    private ABuildingBehaviour buildingToCreate;

    public void CreateBuilding()
    {
        ABuildingBehaviour isIstance = Instantiate(buildingToCreate);
        isIstance.currentState = BuildingPlacingState.Positioning;
    }
}
