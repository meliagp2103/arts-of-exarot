﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Resource",menuName ="Resource/Resource")]
public class Resource : ScriptableObject
{
    public ResourceType resourceType;

    public int amount;

}