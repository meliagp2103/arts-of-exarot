﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ResourceBehaviour : MonoBehaviour
{
    public Resource resourceScriptable;

    private List<PeasentBehaviour> workersExtractingResource = new List<PeasentBehaviour>();

    private float amount;

    private int amountToReach;

    private int countUp = 0;


    private void Awake()
    {
        amount = resourceScriptable.amount;
    }

    public IEnumerator Extract()
    {
        while(true)
        {
            if(countUp < amountToReach)
            {
                countUp++;
            }
            else
            {
                Debug.Log(gameObject.name + "_" + amount);

                foreach(PeasentBehaviour pawn in workersExtractingResource)
                {
                    pawn.currentResourceAmountCarry++;

                    if(pawn.currentResourceAmountCarry == pawn.maxResourceAmount)
                    {
                        //Debug.Log(pawn.name + "wants to deposit stuff");
                        pawn.GoToNearestDeposit();
                    }

                    amount--;

                    if (amount <= 0)
                    {
                        EventManager.resourceDestroyed?.Invoke();
                        Destroy(gameObject);
                        yield break;
                    }
                }
                countUp = 0;
            }
            //TO CHANGE withe the speed of the unit/multiple unit
            yield return new WaitForSeconds(1);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PeasentBehaviour entity))
        {
            Debug.Log("collided with: " + entity.name);

            if (entity.currentResourceAmountCarry == entity.maxResourceAmount)
            {
                entity.GoToNearestDeposit();
            }

            entity.currentUnitState = EntityState.Gathering;

            if (entity.currentResource != this)
            entity.currentResource = this;

            workersExtractingResource.Add(entity);

            entity.resourceTypeThatIsCarrying = resourceScriptable.resourceType;

            amountToReach = entity.maxResourceAmount;

            StartCoroutine("Extract");

            Debug.Log("Extracting");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PeasentBehaviour entity))
        {
            workersExtractingResource.Remove(entity);

            if (workersExtractingResource.Count <= 0)
            {
                StopCoroutine("Extract");

                Debug.Log("Extraction stopped");
            }
        }
    }

}
