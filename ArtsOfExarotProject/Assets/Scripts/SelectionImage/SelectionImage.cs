﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectionImage : MonoBehaviour
{
    public Texture selectionImage;

    private void OnGUI()
    {
        if (!IsValidClick())
        {
            return;
        }

        if (Input.GetMouseButton(0))
        {
            Vector2 endPosition = Input.mousePosition;
            var size = PointerInfo.mousePos - endPosition;
            GUI.DrawTexture(new Rect(PointerInfo.mousePos.x, -PointerInfo.mousePos.y + Screen.height, -size.x, size.y), selectionImage);
        }
    }

    public static bool IsValidClick()
    {
        return !EventSystem.current.IsPointerOverGameObject();
    }
}
