﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ResourceManager : ASingleton<ResourceManager>
{
    public Dictionary<ResourceType, int> resources = new Dictionary<ResourceType, int>();

    public List<PeasentBehaviour> peasents;

    // Start is called before the first frame update
    void Start()
    {
        peasents = FindObjectsOfType<PeasentBehaviour>().ToList();

        resources.Add(ResourceType.Food, 10);
        EventManager.displayResource?.Invoke(ResourceType.Food,10);

        resources.Add(ResourceType.Gold, 0);
        EventManager.displayResource?.Invoke(ResourceType.Gold,0);

        resources.Add(ResourceType.Stone, 0);
        EventManager.displayResource?.Invoke(ResourceType.Stone,0);

        //if this fucking shit is true has to be istantly changed 
        resources.Add(ResourceType.People, peasents.Count);
        EventManager.displayResource?.Invoke(ResourceType.People, peasents.Count);

        resources.Add(ResourceType.Wood, 0);
        EventManager.displayResource?.Invoke(ResourceType.Wood, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddResource(ResourceType resource, int amountToDeposit)
    {
        resources[resource] += amountToDeposit;
        EventManager.displayResource?.Invoke(resource, resources[resource]);
        //Debug.Log($"this {resource} has {resources[resource]}");
    }

    public void RemoveResource(ResourceType resource, int amountToRemove)
    {
        resources[resource] -= amountToRemove;
        EventManager.displayResource?.Invoke(resource, resources[resource]);
        //Debug.Log($"this {resource} has lost {resources[resource]}");
    }
}
