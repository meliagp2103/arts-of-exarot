﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager
{
    public static Action<Vector3> buildingPosition;
    public static Action resourceDestroyed;
    public static Action<ResourceType, int> displayResource;
    public static Action<UIIdentifier> updateSelectedUI;
}
