﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructionSiteBehaviour : MonoBehaviour
{

    public ABuildingBehaviour buildingToBuild;

    public int constructionCountdown;

    private List<PeasentBehaviour> workersInsideTheBuilding = new List<PeasentBehaviour>();

    private float currentCountDown;


    private void Start()
    {
        currentCountDown = constructionCountdown;
    }

    public IEnumerator Construct()
    {
        while(currentCountDown > 0)
        {
            currentCountDown--;
            yield return new WaitForSeconds(1);
        }

        ABuildingBehaviour build = Instantiate(buildingToBuild, transform.position, transform.rotation);
        build.currentState = BuildingPlacingState.Positioned;

        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PeasentBehaviour entity))
        {
            entity.currentUnitState = EntityState.Task;
            workersInsideTheBuilding.Add(entity);
            StartCoroutine("Construct");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PeasentBehaviour entity))
        {
            entity.currentUnitState = EntityState.Idle;

            workersInsideTheBuilding.Remove(entity);

            if (workersInsideTheBuilding.Count <= 0)
            {
                StopCoroutine("Construct");
            }
        }
    }
}
