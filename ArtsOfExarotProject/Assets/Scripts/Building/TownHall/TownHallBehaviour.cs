﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownHallBehaviour : ABuildingBehaviour,IDepositable
{
    public float timerToGeneratePeasents = 1f;
    public GameObject selectedIcon;
    public Transform meetingPoint;
    public bool isSelected;

    private void OnTriggerEnter(Collider other)
    {
        Deposit(other);
    }

    public override void Select()
    {
        base.Select();
        isSelected = true;
        selectedIcon.SetActive(true);
    }

    public override void Deselect()
    {
        base.Deselect();
        isSelected = false;
        selectedIcon.SetActive(false);
    }

    public void Deposit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PeasentBehaviour entity))
        {
            //Add The Amount to the Database (using also the enum fotr that specific resource)
            if (entity.currentResourceAmountCarry > 0)
            {
                ResourceManager.Instance.AddResource(entity.resourceTypeThatIsCarrying, entity.currentResourceAmountCarry);

                entity.currentResourceAmountCarry = 0;

                entity.resourceTypeThatIsCarrying = ResourceType.None;

                if (entity.currentResource != null)
                    entity.Movement(entity.currentResource.transform.position);
            }
            Debug.Log("Deposit");
        }
    }

    public void GenerateNewPawn()
    {

    }
}
