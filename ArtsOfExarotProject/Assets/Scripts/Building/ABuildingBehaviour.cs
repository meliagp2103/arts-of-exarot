﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class ABuildingBehaviour : MonoBehaviour,IDamageable,IMultipleSelectable
{
    public BuildingsStats buildingStats;
    public LayerMask groundMask;
    public BuildingPlacingState currentState;
    public UIIdentifier UIID;
    public GameObject ConstructionSite;
    public resourceCost BuildingCost;

    private bool placeable = false;
    private BoxCollider boxCollider;
    private Vector3 boxColliderCentre;
    private Material buildingMaterial;


    void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        boxColliderCentre = boxCollider.center;
        buildingMaterial = GetComponentInChildren<MeshRenderer>().material;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(currentState == BuildingPlacingState.Positioning)
        {
            PlaceBuilding();
            FindAngles();
        }
    }

    public virtual void Deselect()
    {
        EventManager.updateSelectedUI?.Invoke(UIID);
        Debug.Log("Ouch");
    }

    public virtual void Select()
    {
        EventManager.updateSelectedUI?.Invoke(UIID);
        Debug.Log("Stuff");
    }

    public void TakeDamage(float dmgAmount)
    {
        Debug.Log("Other Stuff");
    }

    public void FindAngles()
    {
        //Debug.Log("Dio Ottovolante");

        Vector3 botLeft = new Vector3(-boxCollider.bounds.extents.x,
            -boxCollider.bounds.extents.y, 
            -boxCollider.bounds.extents.z) 
            + boxColliderCentre;

        Vector3 botRight = new Vector3(boxCollider.bounds.extents.x, 
            -boxCollider.bounds.extents.y, 
            -boxCollider.bounds.extents.z) 
            + boxColliderCentre;

        Vector3 topLeft = new Vector3(boxCollider.bounds.extents.x, 
            -boxCollider.bounds.extents.y, 
            boxCollider.bounds.extents.z) 
            + boxColliderCentre;

        Vector3 topRight = new Vector3(-boxCollider.bounds.extents.x, 
            -boxCollider.bounds.extents.y, 
            boxCollider.bounds.extents.z) 
            + boxColliderCentre;

        //TODO Center Point
        //Vector3 center = new Vector3(boxColliderCentre.x,
        //    -boxCollider.bounds.extents.y,
        //   boxColliderCentre.z)
        //    + boxColliderCentre;

        Debug.DrawRay(gameObject.transform.TransformPoint(botLeft), Vector3.down * 10000f, Color.green);
        Debug.DrawRay(gameObject.transform.TransformPoint(botRight), Vector3.down * 10000f, Color.green);
        Debug.DrawRay(gameObject.transform.TransformPoint(topLeft), Vector3.down * 10000f, Color.green);
        Debug.DrawRay(gameObject.transform.TransformPoint(topRight), Vector3.down * 10000f, Color.green);

        if (Physics.Raycast(gameObject.transform.TransformPoint(botLeft), Vector3.down, out RaycastHit hitBL,0.5f, groundMask) 
            && Physics.Raycast(gameObject.transform.TransformPoint(botRight), Vector3.down, out RaycastHit hitBR, 0.5f, groundMask) 
            && Physics.Raycast(gameObject.transform.TransformPoint(topLeft), Vector3.down, out RaycastHit hitTL, 0.5f, groundMask) 
            && Physics.Raycast(gameObject.transform.TransformPoint(topRight), Vector3.down, out RaycastHit hitTR, 0.5f, groundMask))
        {
            buildingMaterial.color = Color.green;
            placeable = true;
        }
        else
        {
            buildingMaterial.color = Color.red;
            placeable = false;
        }
    }

    public void PlaceBuilding()
    {
        Ray screenPoint = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        //Debug.Log("Dio Ottovolante");

        if (Physics.Raycast(screenPoint, out hitInfo, Mathf.Infinity, groundMask))
        {
            transform.position = hitInfo.point + Vector3.up * (boxCollider.size.y / 2);

            if (Input.GetMouseButtonDown(1))
            {
                //Debug.Log("correct input");
              
                if (placeable)
                {
                    currentState = BuildingPlacingState.Positioned;
                    EventManager.buildingPosition?.Invoke(transform.position);
                    gameObject.SetActive(false);
                    Instantiate(ConstructionSite, transform.position, transform.rotation);
                    Destroy(gameObject);

                }
            }
        }
    }
}

[SerializeField]
public struct resourceCost
{
    public int amount;
    public ResourceType resourceToSubtract;
}

public enum BuildingPlacingState
{
    Positioning,
    Positioned
}