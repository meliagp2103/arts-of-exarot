﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : Area
{
    public Vector3 Center
    {
        get
        {
            var center = startPoint + (endPoint - startPoint) * 0.5f;
            return center;
        }
    }

    public Vector3 Size => new Vector3
    (
        Mathf.Abs(startPoint.x - endPoint.x),
        (startPoint - endPoint).magnitude,
        Mathf.Abs(startPoint.z - endPoint.z)
    );

    public Vector3 Extents => Size * 0.5f;
}
