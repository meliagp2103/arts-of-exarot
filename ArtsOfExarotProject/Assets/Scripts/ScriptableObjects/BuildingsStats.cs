﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Settings/BuildStats",order = 0)]
public class BuildingsStats : ScriptableObject
{
    public float health;

    public BuildingType buildingType;

    public float timeToBuild;

    public float timeToGenerateUnits;


}
public enum BuildingType
{
    Economy,
    Military,
    Research,
    Defensive,
    Miscellaneous
}