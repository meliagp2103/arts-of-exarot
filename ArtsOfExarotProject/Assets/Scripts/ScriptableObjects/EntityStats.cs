﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Custom/Settings/EntityStats", order = 1)]
public class EntityStats : ScriptableObject
{
    public float health;

    public DamageType damageType;

    public float damageAmount;

    public ArmourType armourType;

    public float range;

    public float bonusDamageAgainstArmour;

    public float lineofSight;

    public float movementSpeed;

    public float minimumRange;

    public float accuracy;
}

public enum DamageType
{
    Melee,
    Pierce
}

public enum ArmourType
{
    Melee,
    Pierce
}

public enum EntityType
{
    Economy,
    Military
}