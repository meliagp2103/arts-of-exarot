﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectionManager : ASingleton<SelectionManager>
{
    public List<IMultipleSelectable> selectedList = new List<IMultipleSelectable>();
    private List<AEntityBehaviour> selectedTroops = new List<AEntityBehaviour>();
    private Box selectionBox = new Box();

    private void SingleSelection()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray,out RaycastHit hit, Mathf.Infinity))
        {
            if(hit.collider.gameObject.TryGetComponent<IMultipleSelectable>(out var selected))
            {
                selected.Select();
                selectedList.Add(selected);
            }

            if (hit.collider.gameObject.TryGetComponent<AEntityBehaviour>(out var troop))
            {
                selectedTroops.Add(troop);
                Debug.Log("Added: " + troop.gameObject.name + "into the list");
            }
        }
    }

    private void UpdateAreaCorners(ref Vector3 corner)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity,LayerMask.GetMask("Ground")))
        {
            corner = hit.point;
        }
    }

    private void DrawBox()
    {
        Debug.Log("Drawing The box");

        Collider[] colliders = Physics.OverlapBox(selectionBox.Center, selectionBox.Extents);

        foreach (Collider col in colliders)
        {
            if (col.gameObject.TryGetComponent<IMultipleSelectable>(out var selected))
            {
                selected.Select();
                selectedList.Add(selected);
            }

            if (col.gameObject.TryGetComponent<AEntityBehaviour>(out var troop))
            {
                selectedTroops.Add(troop);
                Debug.Log("Added: " + troop.gameObject.name + "into the list");
            }
        }
    }

    private void ResetSelectedList()
    {
        foreach (IMultipleSelectable selected in selectedList)
        {
            selected.Deselect();
        }

        selectedList.Clear();
        selectedTroops.Clear();
        //Debug.Log("Cleared list");
    }

    public static bool IsValidClick()
    {
        return !EventSystem.current.IsPointerOverGameObject();
    }

    private void Update()
    {
        //Debug.Log(IsValidClick());

        if (!IsValidClick())
        {
            return;
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (selectedTroops.Count > 0)
            {
                //raycast to take the clicked position
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Vector3 destination = Vector3.zero;

                if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, LayerMask.GetMask("Ground")))
                {
                    destination = hit.point;
                }

                for (int i = 0; i < selectedTroops.Count; i++)
                {
                    selectedTroops[i].Movement(destination);
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            UpdateAreaCorners(ref selectionBox.startPoint);
            ResetSelectedList();
            SingleSelection();
            PointerInfo.mousePos = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            UpdateAreaCorners(ref selectionBox.endPoint);
        }

        if (Input.GetMouseButtonUp(0))
        {
            DrawBox();
        }
    }
}
